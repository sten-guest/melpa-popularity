;;; melpa-popularity.el --- get sorted MELPA download stats for a set of packages

;; Copyright: 2018 Nicholas D Steeves

;; Author: Nicholas D Steeves <nsteeves@gmail.com>
;; URL: https://salsa.debian.org/sten-guest/melpa-popularity
;; Version: 0.4
;; Keywords: Tools
;; Package-Requires: ((emacs "25.1"))
;; License: GPL-3+

;; To use, select a list of MELPA package names and run:
;; M-x mpop-from-selection
;; Results are outputted to the *Messages* buffer.

;; Credits for the alist sort are due to Nic Ferrier.  My sort was
;; trivially adapted from his kvalist-sort-by-value function from
;; kv.el.  kv.el is Copyright: 2012 Nic Ferrier, License: GPL-3+

(require 'url)
(require 'json)
(require 'map)
(require 'subr-x)


;; TODO: Customisation group
(setq melpa-url "https://melpa.org/download_counts.json")
;; (setq melpa-url "https://stable.melpa.org/download_counts.json")
(setq mpop-dl-counts-file "~/.cache/MELPA_download_counts.json")

(defun mpop-build-hash-table (json-file)
  "Build a hash table from JSON-FILE.  Use a ht because there are
thousands of key:value pairs of MELPA package:downloads.  Returns
a hash-table"
  (when (and
         (url-copy-file melpa-url mpop-dl-counts-file t)
         (file-readable-p json-file))
    (let ((json-object-type 'hash-table)
          (json-array-type 'list)
          (json-key-type 'symbol))
      (json-read-file json-file))))


(defun mpop-get-quantiles (sample)
  "Calculate quantiles for SAMPLE, where SAMPLE is a hash-table
in the format defined in the mpop-build-hash-table function.
At the moment this function is limited to percentiles.
Returns an alist with the format ( cut-point . quantile )"
  (let ((units (length (hash-table-values sample)))
        (quantiles (list))
        (cut-points (list))
        (count 100))
    (message "Got stats for %i packages" units)
    (when (cl-evenp units)
      (setq units (+ 1 units)))
    ;; determine cut-points
    (while (> count 0)
      (setq count (1- count))
      (push (round (* (/ count 100.0) units)) cut-points))
    ;; Drop the leading value, which is not a cut point
    ;; (cdr cut-points))
    ;; /\ I think this \/ two loop structure implicitly drops the
    ;; leading value, because this worked fine without
    ;; (setq cut-points (cdr cut-points))
    ;; (message "Using %i cut points" (length (cdr cut-points)))
    ;; (message "The cut points are %s" (cdr cut-points))
    (while (< count 100)
      (setq count (1+ count))
      (map-put
       quantiles
       (car
        (seq-subseq (sort
                     (hash-table-values
                      sample) '<)
                    (nth (1- count) cut-points) (nth count cut-points)))
       (1- count)))
    quantiles))

(defun mpop-get-popularity (pkg-list hash-table)
  "Get number of MELPA downloads for a list of packages.
Takes a list of symbols and a hash table in the format defined in
mpop-build-hash-table. Outputs an alist."
  ;; (message "pkg-list is: %s" pkg-list)
  (let  ((output (list)))
    (dolist (element pkg-list output)
      (let ((value (gethash element hash-table)))
        (when value
          (map-put output element value)
          output)))))


(defun mpop-strings-to-symbols (list-of-strings)
  "Converts a list of strings (\"one\" \"two\" \"three\") to
   a list of symbols (one two three)."
  (let (value)
    (dolist (element list-of-strings value)
      (add-to-list 'value (car (read-from-string element))))))


(defun mpop-print-to-buffer (input)
  "Output INPUT to the *MELPA-STATS* buffer."
  (get-buffer-create "*MELPA-STATS*")
  (save-current-buffer
    (switch-to-buffer-other-window "*MELPA-STATS*")
    (set-buffer "*MELPA-STATS*")
    (insert input)
    (newline)))


(defun mpop-from-selection (start end)
  "Gets a list of MELPA packages from selection and writes their
names and number of downloads to the *MELPA-STATS* buffer."
  (interactive "r")
  (let* ((selected-pkgs-symbols
          (mpop-strings-to-symbols
           (split-string (buffer-substring-no-properties start end))))
         (selected-pkgs (sort
                         (mpop-get-popularity selected-pkgs-symbols melpa-pop-ht)
                         (lambda (a b) (funcall '> (cdr a) (cdr b)))))
         (quantile 99))
    (dolist (pkg-with-dls selected-pkgs)
      (while (and
              (<= (cdr pkg-with-dls) (car (nth (- 99 quantile) melpa-pop-q)))
              (>= quantile 0))
        (setq quantile (1- quantile)))
      (mpop-print-to-buffer (message "%s: %i downloads: q%i"
                                     (car pkg-with-dls)
                                     (cdr pkg-with-dls)
                                     quantile)))))


;; things melpa-popularity.el should do once on init
(setq melpa-pop-ht (mpop-build-hash-table mpop-dl-counts-file))
(setq melpa-pop-q (mpop-get-quantiles melpa-pop-ht))

(provide 'melpa-popularity)
